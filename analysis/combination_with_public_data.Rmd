---
title: "Comparison with publicly available datasets "
author: "Reka Toth"
date: "2019-12-12"
output: workflowr::wflow_html
editor_options:
  chunk_output_type: console
---


# Datasets 

## Lung Basal Stem Cells Rapidly Repair DNA Damage Using the Error-Prone Nonhomologous End-Joining Pathway

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5268430/

Here we used flow cytometry to isolate BSCs, luminal (club, goblet, and ciliated) cells, and AT2 cells from fresh human proximal and distal lung tissue and showed that BSCs and AT2 cells behave as progenitor cells in an in vitro colony-forming assay. EPCAM+: alveolar cells (ATII?). Ngfr+ BSCs. luminal cells (club, ciliated, goblet)?

GSE83991

## 	Bronchioalveolar stem cells are a main source for regeneration of distal lung epithelia

Bulk RNAseq of FACS-isolated lung epithelial cell types (BASC/AT2/Club) in duplicates.

GSE129440

## An atlas of the aging lung mapped by single cell transcriptomics and deep tissue proteomics.

Pseudo-bulk data made by combining reads from single cell clusters

GSE124872



# Read in the datasets 


```{r message=FALSE, warning=FALSE}

library(data.table)
library(pheatmap)
library(sva)
library(AnnotationDbi)
library(org.Mm.eg.db)
library(ggfortify)
library(GeneTonic)
# ---------------------folders--------------------

###########libraries and functions#############
if (grepl("Windows", Sys.getenv("OS"))){
  PATH ="V:/"} else {
    PATH ="/C010-projects/"}
if (grepl("Windows", Sys.getenv("OS"))){
  PATH_Y="N:/"} else {
    PATH_Y="/C010-datasets/"}

DATA = paste0(PATH, "Reka/33_CoO_lung/RNASeq_analysis/data/")
RESULT = paste0(PATH, "Reka/33_CoO_lung/RNASeq_analysis/output/")
CODE = paste0(PATH, "Reka/33_CoO_lung/RNASeq_analysis/code/")
CALLS = paste0(PATH_Y, "External/Sotillo_mouse_lung_cell/RNASeq_10_12/")


```

```{r echo=FALSE, message=FALSE, warning=FALSE}


#GSE83991
sorted_cells <- as.data.frame(fread(file.path(DATA, "GSE83991_GeneCounts.txt.gz")))
rownames(sorted_cells) <- sorted_cells$GeneID
sorted_cells <- sorted_cells[,-(1:2)]
sorted_cells <- sorted_cells+1
sorted_cells <- log2(sorted_cells)
batch <- rep("GSE83991", ncol(sorted_cells))
names_batch <- colnames(sorted_cells)

anno_sorted <- data.frame(
          gene_id = rownames(sorted_cells),
          ensembl = mapIds(org.Mm.eg.db,
                             keys = rownames(sorted_cells),
                              column = "ENSEMBL",
                              keytype = "ENTREZID"),
                    gene_name = mapIds(org.Mm.eg.db,
                             keys = rownames(sorted_cells),
                              column =  "SYMBOL",
                              keytype = "ENTREZID"),
          stringsAsFactors = FALSE,
          row.names = rownames(sorted_cells)
)

sorted_cells <- merge(anno_sorted, sorted_cells, by.x="gene_id", by.y="row.names", sort=F, all=F)
sorted_cells2 <- as.data.frame(fread(file.path(DATA, "GSE129440_counts.matrix.norm_anno.txt.gz")))
sorted_cells2 <- sorted_cells2[,c("Ensembl gene id", "Ensembl gene", "AT2_1", "AT2_2","BASC_1", "BASC_2","Club_1","Club_2")]
sorted_cells2[,-(1:2)] <- apply(sorted_cells2[,-(1:2)], 2, function(x) x+1)
sorted_cells2[,-(1:2)] <- apply(sorted_cells2[,-(1:2)], 2, log2)

batch <- c(batch, rep("GSE129440", ncol(sorted_cells2[,-(1:2)])))
names_batch <- c(names_batch, colnames(sorted_cells2[,-(1:2)]))

sorted_cells <- merge(sorted_cells, sorted_cells2, by.x="gene_name", by.y="Ensembl gene", all=T, sort=F)

pseudo_bulk <-  readRDS( file= file.path(DATA, "pseudo_bulk.RDS"))
colnames(pseudo_bulk) <- paste0("pb_", colnames(pseudo_bulk))

sorted_cells <- merge(sorted_cells, pseudo_bulk, by.x="gene_name", by.y="row.names", all=T, sort=F)

batch <- c(batch, rep("pseudo_bulk", ncol(pseudo_bulk)))
names_batch <- append(names_batch, colnames(pseudo_bulk))


ge_mat <- readRDS( file= file.path(DATA, "ge_matrix_counts.RDS"))
#ge_mat <- log2(ge_mat+1)

batch <- c(batch, rep("B220", ncol(ge_mat)))
names_batch <- append(names_batch, colnames(ge_mat))


merged_ge <- merge(sorted_cells, ge_mat, by.x="ensembl", by.y="row.names", all=T, sort=F)
merged_ge <- merged_ge[!is.na(merged_ge$gene_name),]
merged_ge <- merged_ge[!duplicated(merged_ge$gene_name),]
rownames(merged_ge) <- merged_ge$gene_name
merged_ge <- merged_ge[,!(colnames(merged_ge) %in% c("Ensembl gene id", "ensembl", "gene_name", "gene_id"))]


empty <- apply(merged_ge, 1, function(x) sum(is.na(x)))
merged_ge <- merged_ge[empty<4,]




```
## Heatmap before batch effect correction

```{r fig.height=8, fig.width=12, message=FALSE, warning=FALSE}

merged_ge <- merged_ge[
  order(rowSds(as.matrix(merged_ge)), decreasing = T),]


pheatmap(merged_ge[1:5000,], show_rownames = F)

pc <- prcomp(t(merged_ge))

autoplot(pc, data = cbind("Sample"= names_batch, Group=batch), colour="Group")

```

```{r message=FALSE, warning=FALSE}
merged_be_ge <- sva::ComBat(as.matrix(merged_ge), batch = batch)

```


## Heatmap after batch effect correction

```{r fig.height=8, fig.width=12, message=FALSE, warning=FALSE}

merged_be_ge <- merged_be_ge[
  order(rowSds(as.matrix(merged_be_ge)), decreasing = T),]


pheatmap(merged_be_ge[1:5000,], show_rownames = F)

pc <- prcomp(t(merged_be_ge))

autoplot(pc, data = cbind("Sample"= names_batch, Group=batch), colour="Group")
autoplot(pc, data = cbind("Sample"= names_batch, Group=batch), colour="Group", label=T)

```

## Set up the marker lists

Markers are from here:  http://biocc.hrbmu.edu.cn/CellMarker/download.jsp 
```{r message=FALSE, warning=FALSE}


markers <- list()
marker_data <- read.delim2(file.path(DATA, "Mouse_cell_markers.txt"), stringsAsFactors = F)
single_cell <-read.delim2(file.path(DATA, "Single_cell_markers.txt"), stringsAsFactors = F)

marker_data <- marker_data[marker_data$tissueType=="Lung",]
marker_data <- marker_data[marker_data$cellType=="Normal cell",]
marker_data <- marker_data[lapply(strsplit(marker_data$geneSymbol, split = ","), length)>3,]

single_cell <- single_cell[single_cell$speciesType=="Mouse",]
single_cell <- single_cell[single_cell$tissueType=="Lung",]
single_cell <- single_cell[single_cell$cellType=="Normal cell",]
single_cell <- single_cell[lapply(strsplit(single_cell$geneSymbol, split = ","), length)>3,]

#marker_data <- rbind(marker_data, single_cell)
#marker_data <- marker_data[!duplicated(marker_data$geneSymbol),]

for (i in seq_len(nrow(marker_data))){
  markers[[paste0(marker_data$cellName[i], "_", marker_data$PMID[i])]] <- unlist(strsplit(marker_data[i,"geneSymbol"], ", "))
  markers[[paste0(marker_data$cellName[i], "_", marker_data$PMID[i])]] <- gsub("\\]|\\[", "", markers[[paste0(marker_data$cellName[i], "_", marker_data$PMID[i])]])
  markers[[paste0(marker_data$cellName[i], "_", marker_data$PMID[i])]] <- markers[[paste0(marker_data$cellName[i], "_", marker_data$PMID[i])]][markers[[paste0(marker_data$cellName[i], "_", marker_data$PMID[i])]] %in% rownames(merged_be_ge)]
}

markers <- markers[lapply(markers, length)>2]
```


```{r eval=FALSE, include=FALSE}

#https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5268430/
##Ciliated cells 
#markers[["Ciliated cells"]] <- c("1700007G11Rik", "4930451C15Rik", "6820408C15Rik", "CBE1", "Ccdc113", "Ccdc19", "Ccdc39", "Ccdc40", "Ccdc67", "Dnph1", "Efhc1", "Enkur", "Fank1", "Fltp", "Foxj1", "Hs6st2", "Kif9", "Lrrc23", "Lrrc36", "Lrriq1", "Mcidas", "Mlf1", "Ncs1", "Spag17", "Stk33", "Tekt4", "Traf3ip1", "Ttc18", "Wdr16")

#markers[["Ciliated cells"]] <- markers[["Ciliated cells"]][markers[["Ciliated cells"]] %in% rownames(merged_be_ge)]



#Club cells 
#markers[["Club cells"]] <- c("1810010H24Rik", "Aldh1a7", "Cbr2", "Ccnd1", "Ccnd2", "Cd200", "Chad", "Col23a1", "Cyp2f2", "Dcxr", "Gsta3", "Hp", "Itm2a", "Iyd", "Kdr", "Krt15", "Lypd2", "Mia1", "Mtus1", "Nrarp", "Nupr1", "Osgin1", "Pir", "Ppap2b", "Rassf9", "Scgb1a1", "Scgb3a2", "Tacstd2", "Upk3a" )

#markers[["Club cells"]] <- markers[["Club cells"]][markers[["Club cells"]] %in% rownames(merged_be_ge)]



##AT2
#markers[["ATII"]] <- c("Bex2", "Cd36", "Chi3l1", "Chsy1", "Cxcl15", "Dlk1", "Egfl6", "Etv5", "Fabp12", "Fabp5", "Fasn", "Glrx", "Hc", "Il33", "Lamp3", "Lcn2", "Lgi3", "Lyz1", "Lyz2", "Mid1ip1", "Mlc1", "Rab27a", "Retnla", "S100g", "Scd1", "Sftpa1", "Sftpb", "Sftpc", "Slc34a2", "Soat1")
#markers[["ATII"]] <- markers[["ATII"]][markers[["ATII"]] %in% rownames(merged_be_ge)]


```

## Heatmap of the samples using different sets of markers{.tabset .tabset-fade}  

```{r fig.height=8, fig.width=12, message=FALSE, warning=FALSE, results='asis', dev=c('png', 'pdf')}

to_include <- c("Type II pneumocyte_25533958", "Epithelial cell_Company","Fibroblast_26600239" , "Endothelial cell_26600239","Myeloid cell_26600239","Immune cell_26600239", "Epithelial cell_26600239","Endothelial cell_29590628","Myofibroblast_29590628" ,   "Type I pneumocyte_24739965","Type II pneumocyte_24739965","Clara cell_24739965" ,"Ciliated cell_24739965")  
batch <- as.data.frame(batch)

rownames(batch) <- names_batch

for (set in to_include){
data <- merged_be_ge[markers[[set]],]
  cat("\n")
  cat("### ",
      set,
      "\n")
pheatmap(data, scale = "row", main = set, annotation_col = batch)
cat("\n")
}

```
